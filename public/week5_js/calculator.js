function cBtnClick(e) {
    // calculator button clicked
    let clickval = e.innerHTML;

    // React differently on first click after doing a calculation (clicking =)
    if (document.getElementById("calculated").checked){
        if (clickval == '/' || clickval == '*' || clickval =='+') {
            // disable certain operators if value was just calculated
            return;
        } else {
            // otherwise, clear out the cinput
            document.getElementById("cinput").innerHTML='';
            document.getElementById("calculated").checked = false;
        }
    }
    // "Normal" operations of clicking a button
    if (clickval == "C") {
        document.getElementById("cinput").innerHTML=' ';
    } else if (clickval == "=") {
        // Calculate the input, trim long values
        document.getElementById("cinput").innerHTML=Number(eval(document.getElementById("cinput").innerHTML).toFixed(8));
        document.getElementById("calculated").checked=true;
    } else {
        document.getElementById("cinput").innerHTML += clickval;
    }
}
