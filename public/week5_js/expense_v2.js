console.log ('expense_v2.js loaded')
let allExpenses=[[]]

if (localStorage.getItem('expenses'))
    allExpenses = JSON.parse(localStorage.getItem('expenses'))

// clears up data issues before calling populateForm
function populateFormWith(d='2023-03') {
    let thisExpense=[];

    if (d==null) return error;

    if (allExpenses) { thisExpense = allExpenses.filter(function(item) {
        return item[0]== d
    })} 
    console.log(thisExpense.length)
    if (thisExpense.length==0) {
        thisExpense[0] = [d,null,null,null,null,'monthly','Misc ' + d,null,null,null]
    }
    console.log(thisExpense)
    populateForm(thisExpense[0])
}

function populateForm(exp) {
    // add to the form, no check done on data
    if (exp==null) {
        console.log ('Broken in populateForm, null value')
        return
    }
    document.getElementById('date').value = exp[0]
    document.getElementById('mortgage').value = exp[1]
    document.getElementById('gas').value = exp[2]
    document.getElementById('care').value = exp[3]
    document.getElementById('ins').value = exp[4]
    document.getElementById('instype').value = exp[5]
    document.getElementById('misc').value = exp[6]
    document.getElementById('utility').value = exp[7]
    document.getElementById('internet').value = exp[8]
    document.getElementById('mobile').value = exp[9]
}

function processForm (myForm) {
    event.preventDefault
    let one=[]
    // error checking?
    one=[
        document.getElementById('date').value,
        document.getElementById('mortgage').value,
        document.getElementById('gas').value,
        document.getElementById('care').value,
        document.getElementById('ins').value,
        document.querySelector('input[name="instype"]:checked').value,
        document.getElementById('misc').value,
        document.getElementById('utility').value,
        document.getElementById('internet').value,
        document.getElementById('mobile').value
    ]
    // yearly/monthly
    console.log ('Should be yearly or monthly')
    console.log (document.querySelector('input[name="instype"]:checked').value)
    
    if (allExpenses) {
        allExpenses = allExpenses.filter(function(item) {
        return item[0]!== one[0]
    })}
    console.log ('************* pushing onto allExpenses ***************')
    console.log (allExpenses)
    console.log(one)
    if (allExpenses==null) allExpenses=[one]
    else allExpenses.push(one)

    allExpenses.sort((a, b) => {
        if (a[0] < b[0]) {
          return -1;
        }
        if (a[0] > b[0]) {
          return 1;
        }
        return 0;
      });
      

    refreshDisplay()

    // save to localstorage
    var allExpensesJSON = JSON.stringify(allExpenses)
    localStorage.setItem('expenses', allExpensesJSON)

    // clean and put allExpenses to local storage

}

// Takes the contents of allExpenses and recreates the view data
function refreshDisplay () {
    let monthIns, yearIns
    let output=''
    output = `
    <table border=1>
        <tr>
        <th rowspan='2'>Month</th>
        <th rowspan='2'>Mortgage</th>
        <th rowspan='2'>Gas</th>
        <th rowspan='2'>Child Care</th>
        <th colspan="2">Insurance</th>
        <th rowspan=2>Misc Note
        <th colspan="3">Misc</th>
        </tr>
        <tr>
        <th>Monthly</th>
        <th>Yearly</th>
    
        <th>Utility</th>
        <th>Internet</th>
        <th>Mobile</th>
        </tr>`
    if (allExpenses) for (let expArr of allExpenses) {
        if (expArr[5]=='monthly' || expArr[5]=='Monthly') {
            monthIns=expArr[4]
            yearIns= expArr[5]
        }else {
            monthIns= expArr[5]
            yearIns = expArr[4]
        }
    
        output += `
        <tr>
        <td>${new Date('2000-'+Number(expArr[0].slice(-2))+'-01').toLocaleString('default',{month: 'long'})}</td>
        <td>${expArr[1]}</td>
        <td>${expArr[2]}</td>
        <td>${expArr[3]}</td>
        <td>${monthIns}</td>
        <td>${yearIns}</td>
        <td>${expArr[6]}</td>
        <td>${expArr[7]}</td>
        <td>${expArr[8]}</td>
        <td>${expArr[9]}</td>
        </tr>
        `
    }
    output +=`
        </table>
    `;
    document.getElementById('output').innerHTML=output
}


function resetData () {
    console.log('resetting data')
    localStorage.setItem('expenses', null)
    allExpenses=[]
    refreshDisplay()
}