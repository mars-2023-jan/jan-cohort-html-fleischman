// let flag = "false"
// let t = typeof flag
// console.log (typeof( flag) + "   " + t)
// console.log (Boolean(flag))

// age = 14;
// switch (age) {
//     case 60 :
//         console.log('Sixty!')
//         break
//     case 70: 
//         console.log('Seventy')
//         break
//     case (age<18) :
//         console.log ('Minor')
//         break
//     default: 
//         console.log('age is ' + age)
// }


// let i = 0
// while (i<10){
//     console.log('5 * ' + i + " = " + i*5)
//     i++
// }

// for loop
for (let j=1; j<=10; j++) {
    if (!(j%2))
        console.log ( j)
}

// do while loop
let m=1
do {
    console.log(m)
    m++
} while (m>10)
// The contents of thedo loop will always happen at least once, even
//    if the while condition is false to begin with
