const MAX_GUESSES=3
const MAX_ATTEMPTS=3
let nbrGuesses=0
let nbrAttempts
// set the lucky number!
let luckyNumber = Math.floor(Math.random() * 10) + 1

nbrAttempts=1

function setUpGame () {
    nbrGuesses=0
    document.getElementById('status').innerHTML=`Here we go!  You get ${MAX_GUESSES} guesses.`
    // document.getElementById('guess').value=`${luckyNumber}`
    document.getElementById('guess').value=''
    document.getElementById('result').innerHTML=''
    document.getElementById('restart-game').style.display='none'
    document.getElementById('guess').style.display='inline'
    document.getElementById('btn-guess').style.display='inline'
    //document.getElementById('guess').focus() 
}

function checkGuess () {
    let guess=document.getElementById('guess').value
    // Sanity Check on the guess
    console.log ('typeof guess : ' + typeof guess)
    if (guess == '') {
        document.getElementById('status').innerHTML = `You can't leave the guess blank! Try again.<br/> ${MAX_GUESSES-nbrGuesses} guesses left.`
        return
    }
    if (guess < 1 || guess > 10) {
        // out of range
        document.getElementById('status').innerHTML = `${guess} is not between 1 and 10! Try again.<br/> ${MAX_GUESSES-nbrGuesses} guesses left.`
        return
    } 
    document.getElementById('guess').focus() 
    nbrGuesses++
    if (guess == luckyNumber) {
        document.getElementById('status').innerHTML = `${luckyNumber} is the lucky number! You got it on your ${nbrGuesses+getOrdinal(nbrGuesses)} guess on your ${nbrAttempts+getOrdinal(nbrAttempts)} game!`
        document.getElementById('guess').style.display='none'
        document.getElementById('btn-guess').style.display='none'
    } else {
        if (nbrGuesses==MAX_GUESSES) {
            document.getElementById('status').innerHTML = `No more guesses left.`
            if (nbrAttempts < MAX_ATTEMPTS) {
                document.getElementById('guess').style.display='none'
                document.getElementById('btn-guess').style.display='none'
                document.getElementById('restart-game').style.display='block'
            } else {
                document.getElementById('status').innerHTML=`You played ${MAX_ATTEMPTS} games.  No more game attempts left.`
                document.getElementById('guess').style.display='none'
                document.getElementById('btn-guess').style.display='none'
            }
        } else {
            document.getElementById('status').innerHTML = `${guess} is not it. ${MAX_GUESSES-nbrGuesses} guesses left.`
            document.getElementById('guess').value=''
        }
    }
}

function newAttempt() {
    nbrAttempts++
    setUpGame()
    document.getElementById('guess').focus() 
}
function endGame () {
    document.getElementById('status').innerHTML = 'To bad, you didn\'t guess the lucky number.'
    document.getElementById('guess').style.display='none'
    document.getElementById('btn-guess').style.display='none'
    document.getElementById('restart-game').style.display='none'

}
function getOrdinal(n) {
    let ord = '<sup>th</sup>';
    if (n % 10 == 1 && n % 100 != 11)
      ord = '<sup>st</sup>';
    else if (n % 10 == 2 && n % 100 != 12)
      ord = '<sup>nd</sup>';
    else if (n % 10 == 3 && n % 100 != 13)
      ord = '<sup>rd</sup>';
    return ord;
  }
  