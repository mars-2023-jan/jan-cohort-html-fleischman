let names = []
names[0] = 'Aphrodite'
names[1] = 'Jenny'
names[2] = 'Zeus'
names[3] = 'Jarhead'
names[5] = 'John'

console.log('---------ORIGINAL-------')
console.log(names)

let namebob = names.map(bobulate) 
function bobulate (bob) {

    return [bob, 'Bob'+bob]

}
console.log('---------bobbed-------')
console.log(namebob)

// for (let i=0; i< names.length; i++){
//     console.log(names[0])
// }


// let fruits = ['apple','orange']
// for (nm of names) {
//     console.log(nm)
// }
// console.log('------------------')
// names.pop();
// for (nm of names) {
//     console.log(nm)
// }

// console.log('------------------')
// names.push('EGOT')  // push adds a value to end of the array
// names.push(fruits)
// for (nm of names) {
//     console.log(nm)
// }

// fruits.shift() // will remove 

// let arr = new Array("Apple", "Pear", "etc");



