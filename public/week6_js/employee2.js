// if designation is 'Manager" then salary = hoursWorked * 50
// if designation is 'COnsultant" then salary = hoursWorked * 30
// if designation is "Trainee" then salary = hoursWorkded * 20
// Marks is a Manager gets $200000
// firstEmp = new employee(23,'Richard','Trainee',30)
var employees = []

function setupPage () {
    // employees = []
    //restoreEmployees()
    empForm = document.getElementById('employee-form')
    
    employees.push (new Employee(empForm, '12',  'Jeffy', 'Manager', 45))
    employees.push (new Employee(empForm, '14',  'Meghan', 'Consultant', 55))
    employees.push (new Employee(empForm, '16',  'Zoey', 'Trainee', 30))
    employees.push (new Employee(empForm, '18',  'Stuffy', 'Trainee', 35))
    //console.log (employees)
    displayEmployees()
}

class Employee {
    #form
    #id
    #name
    #designation
    #hours
    #salary
    constructor(form, id, name, designation, hours) {
        this.#form = form
        this.#id = id
        this.#name = name
        this.#designation = designation
        this.#hours = hours
        this.#calculateSalary() // calling the calculateSalary function
    }
    // getters and setters
    get id() {
        return this.#id
    }
    set id(newId) {
        this.#id = newId
    }
    get name() {
        return this.#name
    }
    set name(newName) {
        this.#name = newName
    }
    get designation() {
        return this.#designation
    }
    set designation(newDesignation) {
        this.#designation = newDesignation
        this.#calculateSalary() // calling the calculateSalary function
    }
    get hours() {
        return this.#hours
    }
    set hours(newHours) {
        this.#hours = newHours
        this.#calculateSalary() // calling the calculateSalary function
    }
    get salary() {
        return this.#salary
    }
    
    // private method to calculate salary
    #calculateSalary() {
        switch (this.#designation) {
            case 'Manager': 
                this.#salary = this.#hours * 50       
                break
            case 'Consultant':
                this.#salary = this.#hours * 30
                break
            case 'Trainee':
                this.#salary = this.#hours * 20
                break
            default:
                this.#salary = 0
            }   
        return
    }
    
    // method to update form fields
    toForm() {
        this.#form.elements['empId'].value=this.#id
        this.#form.elements["empName"].value=this.#name
        this.#form.elements["designation"].value=this.#designation
        this.#form.elements["hoursWorked"].value=this.#hours
        return;
    }

}

function setActiveEmployeeById(empId) {
    
}


function createEmployee(form) {
    // Get the values of the form fields
    if (!employees) {employees = []}
    emp = new Employee(
        document.getElementById('employee-form'),
        form.elements["empId"].value,
        form.elements["empName"].value,
        form.elements["designation"].value,
        form.elements["hoursWorked"].value,
    )
    // console.log ('*** going to write ***')
    // console.log (emp)

    // must look for that emp ID and replace the record if it exists
    addOrReplaceEmployee(emp)

    // Do something with the employee object (e.g. send it to a server)
    // console.log(emp)
    // console.log(employees)
    
    emp.toForm()
    displayEmployees()
    // echoObject(employee)
  }

// Define a function to add or replace an Employee in the Employees array
function addOrReplaceEmployee(employee) {
    console.log ('----- pushing or replacing -----')
    console.log(employee)
    // Find the index of the Employee with the same empId, if it exists
    const index = employees.findIndex(e => e.id === employee.id);
    if (index === -1) {
        console.log('pushing employee')
        // If the Employee does not exist in the array, add it
        employees.push(employee);
    } else {
        console.log('replacing employee')
        // If the Employee exists in the array, replace it
        employees[index] = employee;
    }
  }

function displayEmployees () {
    console.log ('--- displayEmployees ---')
    if (!employees) return false
    // sort employees, highest salary 1st
    employees.sort(function(a,b) {
        return b.salary-a.salary
    })
    disp='<table border=1>'
    disp+='<tr><th>ID</th><th>Name</th><th>Designation</th><th>Hours</th><th>Salary</th></tr>'
    if (employees) employees.forEach((emp) => {
        disp+=empToHtml(emp)
    })
    disp+='</table>'
    document.getElementById('output').innerHTML = disp
}

function empToHtml(emp) {
    str='';
    str += `<tr><td>${emp.id}</td>
            <td>${emp.name}</td>
            <td>${emp.designation}</td>
            <td>${emp.hours}</td>
            <td>${emp.salary}</td></tr>`
    return str;
}

function saveEmployees() {
    console.log('--- saveEmployees() ---')
    var employeesJSON = JSON.stringify(employees)
    localStorage.setItem('employees', employeesJSON)
}
function restoreEmployees() {
    console.log('--- restoreEmployees() ---')
    console.log(localStorage.getItem('employees'))
    employees = JSON.parse(localStorage.getItem('employees'))
    console.log(employees)
}

function clearEmployees() {
    console.log('--- clearEmployees() ---')
    localStorage.setItem('employees', null)
}



