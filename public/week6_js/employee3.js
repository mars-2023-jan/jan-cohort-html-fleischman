var employees = []
empSelected = {}

function setupPage () {
    // employees = []
    //restoreEmployees()
    empForm = document.getElementById('employee-form')
    
    // add some employees for test data 
    employees.push (new Employee(empForm, '12',  'Jeffy', 'Manager', 45))
    employees.push (new Employee(empForm, '14',  'Meghan', 'Consultant', 55))
    employees.push (new Employee(empForm, '16',  'Zoey', 'Trainee', 30))
    employees.push (new Employee(empForm, '18',  'Stuffy', 'Trainee', 35))

    displayEmployees()

    empSelected = setActiveEmployeeById(16)
}

class Employee {
    #form
    #id
    #name
    #designation
    #hours
    #payrate
    #salary
    constructor(form, id, name, designation, hours) {
        this.#form = form
        this.#id = id
        this.#name = name
        this.#designation = designation
        this.#hours = hours
        this.#calculateSalary() // calling the calculateSalary (#payrate) function
    }
    // getters and setters
    get id() {
        return this.#id
    }
    set id(newId) {
        this.#id = newId
    }
    get name() {
        return this.#name
    }
    set name(newName) {
        this.#name = newName
    }
    get designation() {
        return this.#designation
    }
    set designation(newDesignation) {
        this.#designation = newDesignation
        this.#calculateSalary() // calling the calculateSalary function
    }
    get hours() {
        return this.#hours
    }
    set hours(newHours) {
        this.#hours = newHours
        this.#calculateSalary() // calling the calculateSalary function
    }
    get salary() {
        return this.#salary
    }
    
    // private method to calculate salary
    #calculateSalary() {
        switch (this.#designation) {
            case 'Manager': 
                this.#payrate = 50
                break
            case 'Consultant':
                this.#payrate = 30
                break
            case 'Trainee':
                this.#payrate = 20
                break
            default:
                this.#payrate = 0
            }   
        this.#salary = this.#hours * this.#payrate
        return
    }
    
    // method to update form fields
    toForm() {
        this.#form.elements['empId'].value=this.#id
        this.#form.elements["empName"].value=this.#name
        this.#form.elements["designation"].value=this.#designation
        this.#form.elements["hoursWorked"].value=this.#hours
        return;
    }

}

function setActiveEmployeeById(empId) {
    // get the record from employees that match empId 
    if (empId) 
        empSelected = employees.find(e => e.id === String(empId));
    else 
        empSelected = new Employee(empForm, '',  '', '', '')
    empSelected.toForm()


}


function createEmployee(form) {
    // Get the values of the form fields
    if (!employees) {employees = []}
    emp = new Employee(
        document.getElementById('employee-form'),
        form.elements["empId"].value,
        form.elements["empName"].value,
        form.elements["designation"].value,
        form.elements["hoursWorked"].value,
    )

    // must look for that emp ID and replace the record if it exists
    addOrReplaceEmployee(emp)
    
    emp.toForm()

    displayEmployees()

  }

// Define a function to add or replace an Employee in the Employees array
function addOrReplaceEmployee(employee) {
    console.log ('----- pushing or replacing -----')
    console.log(employee)
    // Find the index of the Employee with the same empId, if it exists
    const index = employees.findIndex(e => e.id === employee.id);
    if (index === -1) {
        console.log('pushing employee')
        // If the Employee does not exist in the array, add it
        employees.push(employee);
    } else {
        console.log('replacing employee')
        // If the Employee exists in the array, replace it
        employees[index] = employee;
    }
  }
  function removeEmployeeById(empid) {
    alert('delete employee requires work')
  }
function displayEmployees () {
    console.log ('--- displayEmployees ---')
    if (!employees) return false
    // sort employees, highest salary 1st
    employees.sort(function(a,b) {
        return b.salary-a.salary
    })
    // const highestSalaryName = Object.keys(salaries).reduce(function(a, b) { 
    //     return salaries[a] > salaries[b] ? a : b 
    // });
    disp='<table border=1 cellpadding=3 cellspacing=0 width=100%>'
    disp+='<tr style="border-bottom: 1px solid black;"><th>ID</th><th>Name</th><th>Designation</th><th>Hours</th><th>Salary</th></tr>'
    if (employees) employees.forEach((emp) => {
        disp+='<tr>'
        disp+=
            `<td>${emp.id}</td>
            <td>${emp.name}</td>
            <td>${emp.designation}</td>
            <td>${emp.hours}</td>
            <td>${emp.salary}</td>`
            disp+=`<td style="text-align:right;">
            <button type="button" class="btn btn-primary btn-sm" onclick="setActiveEmployeeById(${emp.id})" data-bs-toggle="modal" data-bs-target="#empEdit" >
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen" viewBox="0 0 16 16">
        <path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001zm-.644.766a.5.5 0 0 0-.707 0L1.95 11.756l-.764 3.057 3.057-.764L14.44 3.854a.5.5 0 0 0 0-.708l-1.585-1.585z"/>
        </svg> Edit</button>
        <button type="button" class="btn btn-danger btn-sm" onclick="setActiveEmployeeById(${emp.id});removeEmployeeById(${emp.id}); return false" >
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
        </svg></button></td>`
        disp+= `</tr>`
    })

    disp+='</table>'
    document.getElementById('output').innerHTML = disp
}

function saveEmployees() {
    console.log('--- saveEmployees() ---')
    var employeesJSON = JSON.stringify(employees)
    localStorage.setItem('employees', employeesJSON)
}
function restoreEmployees() {
    console.log('--- restoreEmployees() ---')
    console.log(localStorage.getItem('employees'))
    employees = JSON.parse(localStorage.getItem('employees'))
    console.log(employees)
}

function clearEmployees() {
    console.log('--- clearEmployees() ---')
    localStorage.setItem('employees', null)
}



