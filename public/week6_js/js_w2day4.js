
let salaries = {
    "John": 1000,
    "Mary": 800,
    "Carl": 900
}
const highestSalaryName = Object.keys(salaries).reduce(function(a, b) { 
    return salaries[a] > salaries[b] ? a : b 
});

console.log(highestSalaryName)



// function getArr() {
//     return [34,4,644,12,3]
// }

// let numbers = getArr()
// //let [a,b,c,d] = getArr()

// console.log(numbers[0]) // = 34

// let [a,b,...c] = getArr() || [] // puts in an empty array if null

// console.log(c)

// let a1=3.14;
// let b1=2.01;

// [a1,b1] = [b1, a1]

// console.log(undefined)

// spread operator
