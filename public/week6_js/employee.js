
// if designation is 'Manager" then salary = hoursWorked * 50
// if designation is 'COnsultant" then salary = hoursWorked * 30
// if designation is "Trainee" then salary = hoursWorkded * 20


// Marks is a Manager gets $200000

let employees = []

function createEmployee(form) {
    // debugger;
    // Get the values of the form fields
    const empId = form.elements["empId"].value;
    const empName = form.elements["empName"].value;
    const designation = form.elements["designation"].value;
    const hoursWorked = form.elements["hoursWorked"].value;
    let salary


    switch (designation) {
        case 'Manager':
            salary = hoursWorked * 50
            break
        case 'Consultant':
            salary = hoursWorked * 30
            break
        case 'Trainee':
            salary = hoursWorked * 20
            break
        default:
            salary = 'Not Known.  Designation not found.'

    }

    

    // Create a new employee object with the form data
    const employee = {
      empId: empId,
      empName: empName,
      designation: designation,
      hoursWorked: hoursWorked,
      salary: salary,
    };

    addOrReplaceEmployee(employee);

    // Do something with the employee object (e.g. send it to a server)
    console.log(employees)
    
    displayEmployees()
    // echoObject(employee)
  }

// Define a function to add or replace an Employee in the Employees array
function addOrReplaceEmployee(employee) {
    // Find the index of the Employee with the same empID, if it exists
    const index = employees.findIndex(e => e.empID === employee.empID);
  
    if (index === -1) {
      // If the Employee does not exist in the array, add it
      employees.push(employee);
    } else {
      // If the Employee exists in the array, replace it
      employees[index] = employee;
    }
  }
  
function displayEmployee(emp) {
    //str=emp
    str = ''
    str += `${emp.empName} is a ${emp.designation} gets ${emp.salary}`
    str += '\r\n'
    document.getElementById('output').append (str)
    return str
}

function displayEmployees () {
    // sort employees
    employeeSort.sort(function(a,b) {
        return b.salary-a.salary
    })
    // either return the top one only or all of them sorted
    document.getElementById('output').innerHTML= ''
    console.log(employees)
    employeeSort.forEach((emp) => {
        console.log(emp)
        displayEmployee(emp)
    })
    
}

function echoObject (obj) {
    for (item in obj) {
        console.log('----------')
        console.log(item)
    }
}

