let employee = {  // Object literal
    'firstName' : 'John',
    'lastName' : 'Smith',
    'age' : 42,
    'designation' : 'Manager',
    // getFullName : function () { return this.firstName + ' ' + this.lastName},
    getFullName : function () { return this.firstName + ' ' + this.lastName}
}

for (const key in employee) {
    console.log(employee[key])
}
// console.log(typeof employee)

function Car(make, model, year, color) {
    this.make = make
    this.model = model
    this.year = year
    this.color = color
}

let car1=new Car('Toyata','Corolla',2006, 'gold')

console.log(employee.getFullName())// using () is important as it is a function

console.log(car1)
